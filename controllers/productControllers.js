// import the product model to manipulate
const Product = require("../models/Product");
const Orders = require("../models/Orders");


module.exports.addNewProduct = (req, res) => {
        console.log(req.body);

            // use by constructor to create a documenent with follow od schema
    const addNewProduct = Product ({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
    })

    // console.log(addNewProduct) to check the course
    addNewProduct.save()
    .then(result =>res.send(result))
    .catch(error =>res.send(error))
}

module.exports.getAllactiveProducts = (req, res) =>{
        Product.find({isActive:true}) 
        .then(result =>res.send(result))
        .catch(error =>res.send(error))            

}

module.exports.getSingleProducts = (req, res)=>{     
        // console.log(req.params)
        // console.log(req.params.productID) ---> to get us passed as the route

        Product.findById(req.params.productID)
        .then(result =>res.send(result))
        .catch(error =>res.send(error))            

}

// upate product method 
module.exports.updateProducts = (req, res) =>{
        // console.log(req.Orders);
    
        //       

        let update = {
                name:req.body.name,
                description:req.body.description,
                price:req.body.price,
                orders:[
                       {
                        orderId:(req.user.id),
                        productId:(req.user.productId),
                        quantity:(req.user.quantity)    
                       } 
                        
                     ]
           
        }
                 

        // update.push({Orders});

        // const Orders = require("../models/Orders");
        // let order = Orders;

        Product.findByIdAndUpdate(req.params.productID,update,{new:true})
        .then(result =>res.send(result))
        .catch(error =>res.send(error))            

        

}

// archive product method 

module.exports.archiveProduct = (req, res) =>{
        // console.log(req.params.productID);

        let update ={
                isActive : false,
                orders:[
                        {
                         orderId:(req.user.id),
                         productId:(req.user.productId),
                         quantity:(req.user.quantity)    
                        }
                ]
        }

        Product.findByIdAndUpdate(req.params.productID,update,{new:true})
        .then(result =>res.send(result))
        .catch(error =>res.send(error))  
}