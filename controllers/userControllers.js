// the controller of user API

// import the user model in the conrtroller instead because this i where we are now going to use it for

const User = require('../models/User')

//import the package bcrypt 
const bcrypt = require('bcrypt')

const auth = require("../auth")

module.exports.usersRigistration =(req,res) => {
//      console.log(req.body)

    const hahsedPW = bcrypt.hashSync(req.body.password,10);

        let newUser = new User({

            firstName: req.body.firstName,
            lastName:req.body.lastName,
            email: req.body.email,
            password:hahsedPW,
            mobileNo:req.body.mobileNo


        })
        
        // console.log(newUser);
        newUser.save()
        // // console.log(newUser)
        .then(result => res.send(result))
        .catch(error => res.send(error))
}



module.exports.setAdmin = (req,res) => {
     User.findById(req.params.userId)
        .then(foundUser => {
                if(foundUser === null){
                    return res.send({message: "no user found"});
                }
                foundUser.isAdmin = true;
                foundUser.save()
                .then(result => res.send(result))
                .catch(error => res.send(error))
        })

    //  let  x = true;

    // // console.log(req.params.UserId);
    // let update =-{
    //     isAdmin:x
    // }
    // User.findByIdAndUpdate(req.params.UserId,update,{new:true})
    // .then(result => res.send(result))
    // .catch(error => res.send(error))
};




module.exports.logInUser =(req, res) => {
    // console.log(req.body); // checck the input passes via the client 


    User.findOne({email:req.body.email})
    .then(foundUser => {

        if(foundUser === null){
        
                return res.send({message: 'User not found'})
        } else{
            //  console.log("found user")
            //  console.log(foundUser)
            const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)
            console.log(isPasswordCorrect);

            if(isPasswordCorrect){

                // console.log("we will create a token for the users if the password is correct")

                return res.send({accessToken: auth.createAccesToken(foundUser)});
            }
        }
    })

}




module.exports.getUserDetails = (req, res) => {
            //  console.log(req.user);

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))
           
            
}