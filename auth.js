// auth.file

// import this file JWt is to seccure oour password 
const jwt = require("jsonwebtoken");

// create a secret of string the will use to validate pf passeed token 
const secret ="capstone2API"

// create token method for user log in
module.exports.createAccesToken= (userDetails) => {

        const data ={
            id: userDetails.id,
            email:userDetails.email,
            isAdmin: userDetails.isAdmin
        }

        // console.log(data)

        // jwt.sign() will create a JWT our data object, with our secret 
        return jwt.sign(data,secret,{});
}   



// create verify method as middleware

module.exports.verify = (req,res, next) => {

    let token = req.headers.authorization

    if(typeof token ==="undefined"){
        return res.send({auth: "Failed. No Token"})
    }else{
        

        token = token.slice(7);
        //  console.log(token)


        jwt.verify(token,secret,function(error,decodedToken){
                // console.log(decodedToken);
                // console.log(err)

              if(error){
                return res.send({
                    auth:"Failed",
                    message:error.message

                }); 

              }else{
                req.user = decodedToken;
                next();
              } 
             
        });
    }




}


//verifyAdmin will be used as a middleware.
//It has to follow or be added after verify(), so that we can check for the validity and add the decodedToken is the request object as req.user.

module.exports.verifyAdmin = (req, res, next)=>{
  // console.log(req.user);

  if(req.user.isAdmin){
      next();
  }else{
     return res.send({

			auth:"Failed",
			message: "Action Forbidden"

		})

  }


}