//  make an and API
const express = require('express') 
const app = express();
const port = 4000;
// immporting mongoose  is an ODM library API manipulation the MongoDb database
const mongoose = require('mongoose');

mongoose.connect("mongodb+srv://admin:admin123@cluster0.uctt7.mongodb.net/Capstone2API?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true,

});


// create notification is the connection to the db is a sucess or failed 
let db = mongoose.connection;

db.on('error',console.error.bind(console,"MongoDB Connection Error"));

//if the connection is succesful, we wil out put a message in the terminal/gitbash

db.once('open',()=>console.log("Connected to MongoDB"));

// to able handle the request body and parse into  JS object
app.use(express.json());

// import our users router and use it as middleware
const userRoutes = require('./routes/userRoutes')

app.use('/users',userRoutes);

// import product router adn use it as midleware
const productRouter = require('./routes/productRoutes')
app.use('/product',productRouter)


// import orders router 
const orderRouter = require('./routes/orderRoutes')
app.use('/orders',orderRouter)


app.listen(port,()=> console.log("Server is running at port 5000!"));



































// // we will create notofication if the connection to the db is succes  or failed
// let db = mongoose.connection; /// -> store connection or initialize the db variable

// // this is to show notofication of an internal server error from Mongodb
// db.on('error',console.error.bind(console,"MongoDB Connection Error"));

// //if the connection is open add succesful, we will out put a message in the terminal/gitbash
// db.once('open',()=>console.log("Connected to MongoDB."));

// //to be able to handle to request body and parse it into Object


// // hande to request body and parse into Js object
// app.use(express.json()); 


// // import user routes and use ti as ,middleware

// const userRoutes = require("./routes/userRoutes");
// app.use('/users',userRoutes);

// const productRoutes = require("./routes/productRoutes");
// app.use('/products',productRoutes);

// app.listen(port,()=> console.log("Server is running at port 4000!"));




