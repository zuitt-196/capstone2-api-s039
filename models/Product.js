
const mongoose = require('mongoose');

let productScema = new mongoose.Schema({

            name:{
                type: String,
                required:[true,"Name is required"]
            },
            description:{
                type: String,
                required:[true,"Description is required"]
            },
            price:{
                type: Number,
                required:[true,"Price is required"]
            },
            isActive:{
                type: Boolean,
                default: true

            },

            createdOn: {
                type: Date,
                default: new Date()
            },
            orders:[
                {       
                    defaulId:{
                        type:String,
                        required:[true ,"ProductId is required"]
                    },
                    orderId:{
                            type:String,
                            required:[true ,"orderId is required"]
                    },
                    quantity:{
                        type:Number,
                        default:1
                    }
                }
            ]


})


module.exports = mongoose.model("Product",productScema);