// make model or scheama od User

const mongoose = require('mongoose')

let userSchema = new mongoose.Schema({
         
    firstName:{
        type: String,
        required:[true,"First name is required"]
    },
    lastName:{
        type: String,
        required:[true,"Last name is required"]
    },
    email:{
        type: String,
        required:[true,"Email address is required"]
    },
    password:{
        type: String,
        required:[true,"Password is required"]
    },
    mobileNo:{
        type:Number,
        required:[true,"Mobile number is required"]
    },

    isAdmin:{
        type:Boolean,
        default:false
    }

})

module.exports = mongoose.model("User",userSchema);