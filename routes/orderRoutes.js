const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Create order Route
router.post("/",verify,orderControllers.createOrders);

// getUserOrders Route
router.get("/getUserOrders",verify,orderControllers.getUserOrders);


// retrievAllOrders Route
router.get("/getAllOrders",verify,orderControllers.getAllOrders);


//Display product per route order Route
router.get("/productPerOrder/:orderId",verify,verifyAdmin,orderControllers.productPerOrder);

module.exports = router