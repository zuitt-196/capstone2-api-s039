
const express=require('express');
const router = express.Router();

const productController = require('../controllers/productControllers')

const auth = require('../auth')
// import the auh
const {verify,verifyAdmin} = auth;

// addNewProduct route 
router.post('/addProduct',verify,verifyAdmin,productController.addNewProduct)

//Retrieve all Products route
router.get('/getAllactiveProducts',productController.getAllactiveProducts)

// get single Product route
router.get('/getSingleProducts/:productID',verify,verifyAdmin,productController.getSingleProducts)

// update Product route route
 router.put('/updateProduct/:productID',verify,verify,productController.updateProducts)


 //archive a single Product route
 
 router.delete('/archiveProduct/:productID',verify,verify,productController.archiveProduct)



module.exports = router