const express = require('express')
const router =  express.Router();


// the busssines logic of the API should be in controllers
//import the controllers:
const userContollers = require('../controllers/userControllers')
//   console.log('userContollers')

const auth = require('../auth')


const {verify,verifyAdmin} = auth;


// rigster user routes 
router.post('/',userContollers.usersRigistration);

// userlogin route route
router.post('/login',userContollers.logInUser);


// set as Admin route
router.put('/setAdmin/:userId',verify,verifyAdmin,userContollers.setAdmin);



// get usrdatails route
router.get('/getUserDetails',verify,userContollers.getUserDetails);









module.exports = router;